<?php
use Migrations\AbstractMigration;

class PostgresqlEnsureIdUuid extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $tables = [
            'account_settings',
            'action_logs',
            'actions',
            'authentication_tokens',
            'comments',
            'entities_history',
            'favorites',
            'file_storage',
            'gpgkeys',
            'groups',
            'groups_users',
            'organization_settings',
            'permissions',
            'permissions_history',
            'profiles',
            'resource_types',
            'resources',
            'roles',
            'secret_accesses',
            'secrets',
            'secrets_history',
            'user_agents',
            'users'
        ];
        if ($this->getAdapter()->getOptions()["adapter"] === "pgsql")   {
            $this->execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
            foreach ($tables as $table) {
                var_dump($table);
                $this->execute("ALTER TABLE ".$table." ALTER COLUMN id set default uuid_generate_v4()");
            }
        }
    }
}
